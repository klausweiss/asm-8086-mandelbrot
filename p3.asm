.286
.387

.model small, stdcall, nearstack       ; memorymodel, langtype, stackoption: https://msdn.microsoft.com/nl-nl/ss9fh0d6
    ; dzieki stdcall moge robic invoki:
    ; invoke:
    ;    - PUSHuje BP
    ;    - MOV BP, SP
    ;    - do parametrow odwoluje sie przez MOV AX, [BP + 4/6/8/...]
    ;    ... [procedura]
    ;    - popuje BP
    ;    - RET [liczba bajtow] ; - przy wordach 2* liczba parametrow

; DATA SEGMENT ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    data SEGMENT
        ; CONSTANTS
        STACK_SIZE      EQU 512
        MAX_USER_INPUT  EQU 33    ; maksymalna liczba znakow przy przerwaniu
        SGN_MINUS       EQU 0
        SGN_PLUS        EQU 1
        SCREEN_W        EQU 320
        SCREEN_H        EQU 200
        MAX_ITERATIONS  EQU 1000

            ; COLORS
            WHITE   EQU     0Fh
            BLACK   EQU     00h

        ; COORDS
        x_min    DQ -2.0
        x_max    DQ  1.0
        y_min    DQ -1.0
        y_max    DQ  1.0
        p        DQ -1.990625   ; tak samo jest po pierwszej iteracji
        q        DQ  1.0

        ; KOPROCEK HELPERS
        TEN     DQ    10.0
        TENTH   DQ    0.1
        FOUR    DQ    4.0

        ; buffers
        wBuffer    DW    ?

        ; KEYBOARD INPUT
        user_input    DB    MAX_USER_INPUT             ; max liczba znakow
                    DB    ?                        ; ile znakow wprowadzono
                    DB    MAX_USER_INPUT DUP(0)    ; do ładowania przerwaniem 0Ah
                    DB "$"                        ; do wypisywania

        ; MESSAGES
        give_args    DB "Podaj kolejno: x_min, x_max, y_min, y_max: ", "$"

        ; ERRORS
        error_input_format    DB "Podaj argumenty w formacie [+-][0-9]+\.[0-9]+", 10, 13, "$"

    data ENDS

; CODE SEGMENT ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    code SEGMENT
        ASSUME DS: data
        ASSUME SS: stack

    ; ==================================================================================================
    ; ===========================                  MAKRA                ================================
    ; ==================================================================================================

        ; *****************************************
        ; **************** UTILS ******************
        ; *****************************************

            ; ======== check if number =================
            CheckIfNumber MACRO vALue
                ; wywala error, jesli w value nie ma cyfry w ASCII
                CMP vALue, "0"
                JB errorInputFormat        ; mniej niz 0
                CMP vALue, "9"
                JA errorInputFormat        ; wiecej niz 9
            ENDM

            ; ======== ZERO register ===================
            ZERO MACRO reg
                ; zeruje rejestr
                XOR reg, reg
            ENDM
            ; ======== "0" -> 0 in reg register ========
            AsciiToNumber MACRO reg
                SUB reg, "0"
            ENDM

            ; ======== wait for input ==================
            WaitForInput MACRO
                ZERO AH                     ;
                INT 16h
            ENDM

        ; *****************************************
        ; **************** JUMPS ******************
        ; *****************************************

            ; ======== jump if equals ===================
            JMPifEQ MACRO value1, value2, function
                CMP value1, value2
                JE function        
            ENDM

            ; ======== jump if equals ===================
            JMPifNEQ MACRO value1, value2, function
                CMP value1, value2
                JNE function        
            ENDM


        ; *****************************************
        ; *************** KOPROCEK ****************
        ; *****************************************

            ; ======== POP fstack ===================
            FPOP MACRO
                ; usuwanie wartosci z topu fstosu
                FSUBP st(0), st(0)
            ENDM

            ; ======== st(0) <- 0 ===================
            FZERO MACRO
                ; nadpisywanie st(0) na 0
                FSUB st(0), st(0)
            ENDM

            ; ======== st(0) <- st(0)**2 ===================
            FSQUARE MACRO
                ; kwadratowienie topu fstosu
                FMUL st(0), st(0)
            ENDM


        ; *****************************************
        ; *************** GRAPHICS ****************
        ; *****************************************

            ; ======== graphical mode on ===================
            GraphicalModeOn MACRO 
                PUSH AX

                MOV AL, 13h
                ZERO AH
                INT 10h
                
                POP AX   
            ENDM

            ; ======== graphical mode oFF ===================
            GraphicalModeOff MACRO 
                PUSH AX

                MOV AL, 03h
                ZERO AH
                INT 10h

                POP AX   
            ENDM

            ; ======== change color  =======================
            Color MACRO color
                MOV AL, color
            ENDM

            ; ======== paint pixel  =======================
            PaintPixel MACRO
                MOV AH, 0Ch         ; w ramach przyspieszenia mozna to
                                    ; wyjac poza makro i raz ustawiac
                INT 10h
            ENDM

            ; ======== map AX to color  =======================
            MapAXToColor MACRO
                PUSH DX
                MOV DX, MAX_ITERATIONS
                SUB DX, AX
                MOV AX, DX
                MOV DL, 020h
                DIV DL
                POP DX
            ENDM

            ; ======== map AX to color  =======================
            MapAXToColorMono MACRO
                ; macro zastepcze - rysuje na czarno-bialo zamiast na kolorowo
                LOCAL beWhite, beBlack
                CMP AX, MAX_ITERATIONS
                JAE beWhite

                Color BLACK
                JMP beBlack
                beWhite:

                Color WHITE
                beBlack:
            ENDM


    ; ==================================================================================================
    ; ===========================              PROCEDURY                ================================
    ; ==================================================================================================

        ; ---------------------        PRINT            ------------------

            ; -----------------        DEKLARACJE
                Print             PROTO string     : ptr byte
                PrintChar         PROTO char          : byte
                PrintNewLine      PROTO
                PrintQWord        PROTO qw         : ptr qword

            ; ========= print string ============
            Print PROC string : ptr byte
                PUSH DX
                PUSH AX

                MOV AH, 09h
                ZERO DH
                MOV DL, byte ptr [string]
                INT 21h

                CALL PrintNewLine

                POP AX
                POP DX
                RET
            Print ENDP

            ; ========= print char ==============
            PrintChar PROC char : byte
                PUSH DX
                PUSH AX

                MOV AH, 02h
                MOV DL, char
                INT 21h

                POP AX
                POP DX
                RET
            PrintChar ENDP

            ; ========= print new line ==========
            PrintNewLine PROC
                INVOKE PrintChar, 10
                INVOKE PrintChar, 13
                RET
            PrintNewLine ENDP

        ; ---------------------        KOPROCESOR        ------------------

            ; -----------------        DEKLARACJE
                FPushDigit  PROTO  number : word
                FPushFrac   PROTO  number : word

            ; ========= push digit ============
            FPushDigit    PROC  number : word
                ; st(0) *= 10, st(0) += number
                PUSH AX

                FLD qword ptr [TEN] ;
                FMUL                ; st(0) *= 10

                MOV  AX, number             ;
                MOV  word ptr [wBuffer], AX ;
                FILD word ptr [wBuffer]     ;
                FADD                        ; st(0) += number

                POP AX
                RET
            FPushDigit    ENDP

            ; ========= shift right ============
            FPushFrac    PROC     number : word
                ; st(0) = 0.0(..)01
                ; st(1) = czesc dziesietna
                PUSH AX

                MOV  AX, number             ;
                MOV  word ptr [wBuffer], AX ;
                FILD word ptr [wBuffer]     ; laduj number
                FMUL st(0), st(1)           ; number = 0.0..01 * number
                FADDP st(2), st(0)          ; decimal += number*..
                FLD qword ptr [TENTH]       ;
                FMUL                        ; 0.1 -> 0.01

                POP AX
                RET
            FPushFrac     ENDP

        ; ---------------------        ARGUMENTY        ------------------

            ; -----------------        DEKLARACJE
                LoadArgs    PROTO
                LoadArg     PROTO arg : ptr qword
                ReadArg     PROTO

            ; ========== load args     ==============
            LoadArgs    PROC
                ; wczytuje x_min, x_max, y_min, y_max
                INVOKE Print, OFFSET give_args

                INVOKE LoadArg, OFFSET x_min
                INVOKE LoadArg, OFFSET x_max
                INVOKE LoadArg, OFFSET y_max    ; tak na prawde wczytuje na odwrot, bo os OY
                INVOKE LoadArg, OFFSET y_min    ; rosnie w dol
                RET
            LoadArgs    ENDP

            ; ========== load arg     ==============
            LoadArg    PROC arg : ptr qword
                PUSH AX
                PUSH BX
                PUSH CX
                PUSH DX
                PUSH SI

                CALL ReadArg    ; wczytuje arg do user_input
                CALL PrintNewLine

                ZERO CH                     
                LEA SI, user_input             
                MOV CL, byte ptr [SI + 1]    ; CX <- liczba wczytanych znakw
                ADD SI, 2                    ; SI <- user_input

                ; znak (BL):
                ;    0 - minus
                ;    1 - plus
                JMPifEQ byte ptr [SI], "-", processSignMinus    ; jesli -
                MOV BL, SGN_PLUS                                ; jesli +
                JMP processNumber

                processSignMinus:
                MOV BL, SGN_MINUS
                DEC CX
                INC SI

                processNumber:
                ; liczby beda przetwarzane na koprocku

                FLDZ                    ; st(0) = 0

                processCharacteristic:
                    JMPifEQ         byte ptr [SI], ".", processPeriod
                    CheckIfNumber   byte ptr [SI]
                    ZERO AH                     ;
                    MOV  AL, byte ptr [SI]      ;    cyfra do AX
                    AsciiToNumber AX

                    INVOKE FPushDigit, AX

                    INC SI
                LOOP processCharacteristic

                ; nie bylo kropki
                JMP processLoaded

                processPeriod:

                DEC CX
                MOV DH, CL         ; DH <- ilosc cyfr po przecinku

                FLDZ                            ; st(1) = 0
                FLD qword ptr [TENTH]           ; st(0) = 0.1
                processMantissa:
                    INC SI
                    CheckIfNumber byte ptr [SI] ;
                    ZERO AH                     ;
                    MOV  AL, byte ptr [SI]      ;
                    AsciiToNumber AX            ; to by mozna dac do makra, ale nie mam pomyslu na nazwe

                    INVOKE FPushFrac, AX

                LOOP processMantissa

                FPOP        ; zrzuc 0.1
                FADD        ; cecha + mantysa

                processLoaded:
                ; zmiana znaku na -
                JMPifEQ BL, SGN_PLUS, doNotChangeSign
                FCHS            ; zmien znak
                doNotChangeSign:

                MOV SI, word ptr [arg]  ; dostalem adres, zrzucam go do SI
                FSTP qword ptr DS:[SI]  ; pod adres wrzucam wynik

                POP SI
                POP DX
                POP CX
                POP BX
                POP AX
                RET
            LoadArg    ENDP

            ; ========== read arg     ==============
            ReadArg PROC
                ; wczytuje argument z klawiatury do  user_input
                PUSH AX
                PUSH DX

                MOV AH, 0Ah            ; pobierz stringa z klawiatury
                LEA DX, user_input     ; tu go pobierz
                INT 21h                ; pobierz

                POP DX
                POP AX
                RET
            ReadArg ENDP

        ; ---------------------         TRYB GRAFICZNY  -------------------

            ; -----------------        DEKLARACJE
                PaintSet        PROTO
                ComputeRelative PROTO   min : qword, max : qword, N : word, resolution : word, target : ptr qword
                ProcessPoint    PROTO 

            ; ==========    paint set     ==============
            PaintSet    PROC
                ; rysuje na ekranie zbior Mandelbrota
                PUSH AX     ; zmieniam AX w ProcessPoint
                PUSH CX
                PUSH DX

                ZERO CX
                ZERO DX

                loopY:
                    CMP DX, SCREEN_H
                    JAE setPainted

                    loopX:
                        CMP CX, SCREEN_W
                        JAE nextRow
                        ; CX <- N
                        ; DX <- M

                        INVOKE ComputeRelative, x_min, x_max, CX, SCREEN_W, OFFSET p ; policz p
                        INVOKE ComputeRelative, y_min, y_max, DX, SCREEN_H, OFFSET q ; policz q

                        CALL ProcessPoint   ; AX <- iter

                        MapAXToColor
                        PaintPixel


                        INC CX
                        JMP loopX
                    nextRow:
                    ZERO CX
                    INC DX
                JMP loopY

                setPainted:

                POP DX
                POP CX
                POP AX
                RET
            PaintSet    ENDP

            ; ==========    compute relative    ========
            ComputeRelative PROC   min : qword, max : qword, N : word, resolution : word, target : ptr qword
                PUSH SI

                FLD  max        ; st(4) <- max
                FLD  min        ; st(3) <- min
                FILD resolution ; st(2) <- 
                FILD N          ; st(1) <- N
                FLDZ            ; st(0) <- 0

                FADD st, st(4)  ; st(0) <- max
                FSUB st, st(3)  ; st(0) <- max-min
                FMUL st, st(1)  ; st(0) <- N*(max-min)
                FDIV st, st(2)  ; st(0) <- N*(max-min)/resolution
                FADD st, st(3)  ; st(0) <- N*(max-min)/resolution + min

                MOV SI, word ptr [target]   ; dostalem adres, zrzucam go do SI
                FSTP qword ptr DS:[SI]      ; pod adres wrzucam wynik

                FPOP
                FPOP
                FPOP
                FPOP

                POP SI
                RET
            ComputeRelative ENDP

            ; ==========    iteration loops    ========
            ProcessPoint PROC
                ; liczy, czy jestem w stanie przyblizyc punkt MAX_ITERATIONS iteracjami
                ; (czyli ta wewnetrzna petla)
                PUSH CX
                ; nie PUSHuje AX, bo zwracam przez niego wartosc
                ; do mapowania na kolor

                FPOP
                FPOP
                FLD qword ptr DS:[p]    ; st(7) <- p
                FLD qword ptr DS:[q]    ; st(6) <- q
                FLDZ                    ; st(5) <- x
                FLDZ                    ; st(4) <- x**2
                FLDZ                    ; st(3) <- y
                FLDZ                    ; st(2) <- y**2
                FLDZ                    ; st(1) <- tmp
                FLDZ                    ; st(0) <- tmp2 [do obliczen]

                MOV CX, MAX_ITERATIONS
                processPointIter:
                    ; tmp
                    FADD st, st(4)  ; tmp2 <- x**2
                    FSUB st, st(2)  ; tmp2 <- x**2 - y**2
                    FADD st, st(7)  ; tmp2 <- x**2 - y**2 + p
                    FXCH st(1)      ; tmp <- tmp2
                    FZERO       ; tmp2 <- 0
                    ; y
                    FADD st, st(3)  ; tmp2 <- y
                    FMUL st, st(5)  ; tmp2 <- x*y
                    FADD st, st     ; tmp2 <- 2*x*y
                    FADD st, st(6)  ; tmp2 <- 2*x*y + q
                    FXCH st(3)      ; y <- tmp2
                    FZERO       ; tmp2 <- 0
                    ; y**2
                    FADD st, st(3)  ; tmp2 <- y
                    FSQUARE         ; tmp2 <- tmp2**2
                    FXCH st(2)      ; y**2 <- tmp2
                    FZERO       ; tmp2 <- 0
                    ; x
                    FADD st, st(1)  ; tmp2 <- tmp
                    FXCH st(5)      ; x <- tmp2
                    FZERO       ; tmp2 <- 0
                    ; x**2
                    FADD st, st(5)  ; tmp2 <- x
                    FSQUARE         ; tmp2 <- tmp2**2
                    FXCH st(4)      ; x**2 <- tmp2
                    FZERO       ; tmp2 <- 0
                    ; warunek
                    FADD st, st(4)
                    FADD st, st(2)
                    FCOM qword ptr DS:[FOUR]        ; ? x**2 + y**2 > 4.0
                                                    ; mozna by zaladowac 1 i 2 razy pomnozyc x2, zeby bylo 4
                                                    ; ale i tak musze zrzucic flagi z koprocka, korzystajac z pamieci
                                                    ; wiec tak nie robie
                                                    ; robiac tak, nie trzymalbym x**2 i y**2 na stosie koprocka
                                                    ; liczbylym w trakcie liczenia warunku
                                                    ; a na dole trzymalbym 4
                    FSTSW word ptr DS:[wBuffer]     ; zrzuc flagi koprocka do pamieci
                    MOV AX, word ptr DS:[wBuffer]   ; AX <- flagi
                    SAHF                            ; flagi procka <- AX
                    JA processPointBreak            ; warunek spelniony

                    FZERO       ; tmp2 <- 0
                LOOP processPointIter

                processPointBreak:
                MOV AX, MAX_ITERATIONS  ; 
                SUB AX, CX              ; AX <- ile iteracji przeszedlem

                FPOP
                FPOP
                FPOP
                FPOP
                FPOP
                FPOP
                FPOP
                FPOP

                POP CX
                RET 
            ProcessPoint ENDP



    ; ==================================================================================================
    ; ===========================          PROGRAM PROGRAM            ==================================
    ; ==================================================================================================

        ; ============= main =================
        main PROC
            ; ================================

            ; ### INIT
                ; # stos
                MOV AX, stack
                MOV SS, AX
                LEA SP, peek

                ; # data segment
                MOV AX, data
                MOV DS, AX

                ; # koprocek
                FINIT

            ; ### FLOW
                CALL LoadArgs
                GraphicalModeOn
                CALL PaintSet
                WaitForInput
                GraphicalModeOff

                JMP exit
        main ENDP


    ; ==================================================================================================
    ; ===========================                 UJSCIA                ================================
    ; ==================================================================================================


        errorInputFormat:
            LEA DX, error_input_format     ; komunikat bledu
            JMP exitWithMessage            ; exit

        exitWithMessage:
            MOV AH, 09h         ; 09h - wyswietla ciag znakow z adresu pod DX
            INT 21h             ; 21h - przerwanie programu z instrukcja z AH
            JMP exit

        waitAndExit:
            WaitForInput
            JMP exit

        exit:
            MOV    AH, 04Ch     ; 4Ch - konczy program i wraca do DOSu
            INT    21h          ; 21h - przerwanie programu z instrukcja z AH

    code ENDS

; STACK SEGMENT ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    stack SEGMENT STACK

                DW STACK_SIZE dup(?)    ; stos STACK_SIZE-elementowy
        peek    DW ?                    ; wierzcholek
    stack ENDS

; END PROGRAM ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    END main
